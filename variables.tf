variable "project"
{
  default = "misows-hyperflow"
}

variable "region"
{
  default = "europe-west3"
}

variable "zone"
{
  default = "europe-west3-c"
}

variable "gke_cluster_name" {
  default = "gke-test-cluster-hyperflow"
}


#address to influx db
#example default = "http://ec2-18-219-231-96.us-east-2.compute.amazonaws.com:8086/hyperflow_tests"
#http://<url>:8086/<database>
variable "influx_db_url"
{
  default = ""
}

variable "hyperflow_master_container"
{
  default = "sebaciv/hyperflow-master:latest"
}

variable "hyperflow_worker_container"
{
  default = "krysp89/hyperflow-worker-nfs:latest"
}

#change to "ENABLED" for feature to start working
variable "feature_download"
{
  default = "DISABLED"
}

variable "server_port" {
  description = "The port the server will use for rabbitmq"
  default = 5672
}

variable "server_plugin_port" {
  description = "rabbitmq menagement plugin"
  default = 15672
}
