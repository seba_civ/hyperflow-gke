## set region
provider "google" {
  project = "${var.project}"
  region = "${var.region}"
}

## GKE Cluster
resource "google_container_cluster" "primary" {
  name = "${var.gke_cluster_name}"
  zone = "${var.zone}"
  initial_node_count = 2

  master_auth {
    username = "mr.yoda"
    password = "adoy.rm123456789"
  }
}

data "google_container_cluster" "primary" {
  name   = "${var.gke_cluster_name}"
  zone   = "${var.zone}"
}

output "client_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.client_certificate}"
}

output "client_key" {
  value = "${google_container_cluster.primary.master_auth.0.client_key}"
}

output "cluster_ca_certificate" {
  value = "${google_container_cluster.primary.master_auth.0.cluster_ca_certificate}"
}

output "endpoint" {
  value = "${google_container_cluster.primary.endpoint}"
}

provider "kubernetes" {
  host = "${google_container_cluster.primary.endpoint}"
  username = "mr.yoda"
  password = "adoy.rm123456789"
  //client_certificate = "${base64decode(data.google_container_cluster.primary.master_auth.0.client_certificate)}"
  //client_key = "${base64decode(data.google_container_cluster.primary.master_auth.0.client_key)}"
  cluster_ca_certificate = "${base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)}"
}
