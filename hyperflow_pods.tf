resource "kubernetes_pod" "hyperflow-master" {
  metadata {
    name = "hyperflow-master"
    labels {
      App = "hyperflow-master-app"
    }
  }

  spec {
    container {
      image = "${var.hyperflow_master_container}"
      name = "hyperflow-master"

      env {
        name = "INFLUX_DB"
        value = "${var.influx_db_url}"
      }

      port {
        container_port = "${var.server_port}"
      }
    }
  }
}

resource "kubernetes_service" "hyperflow-master-service" {
  metadata {
    name = "hyperflow-master-service"
  }
  spec {
    selector {
      App = "${kubernetes_pod.hyperflow-master.metadata.0.labels.App}"
    }
    port {
      port = "${var.server_port}"
      target_port = "${var.server_port}"
    }

    type = "LoadBalancer"
  }
}

resource "kubernetes_pod" "hyperflow-worker" {
  metadata {
    name = "hyperflow-worker"
    labels {
      App = "hyperflow-worker-app"
    }
  }

  spec {
    container {
      image = "${var.hyperflow_worker_container}"
      name = "hyperflow-worker"

      env {
        name = "INFLUX_DB"
        value = "${var.influx_db_url}"
      }
      env {
        name = "AMQP_URL"
        value = "amqp://${kubernetes_service.hyperflow-master-service.load_balancer_ingress.0.ip}:${var.server_port}"
      }
      env {
        name = "STORAGE"
        value = "local"
      }
      env {
        name = "FEATURE_DOWNLOAD",
        value = "${var.feature_download}"
      }

      volume_mount {
        mount_path = "/data"
        name = "data-volume"
      }
    }
    volume {
      name = "data-volume"
      persistent_volume_claim {
        claim_name = "${kubernetes_persistent_volume_claim.default.metadata.0.name}"
      }
    }
  }

  depends_on = [
    "kubernetes_service.hyperflow-master-service",
    "kubernetes_persistent_volume_claim.default",
  ]
}
