resource "kubernetes_persistent_volume_claim" "default" {
  metadata {
    name = "data-disk-claim"
  }
  spec {
    access_modes = ["ReadWriteOnce"]
    resources {
      requests {
        storage = "10Gi"
      }
    }
    storage_class_name = "${kubernetes_storage_class.default.metadata.0.name}"
  }
}

resource "kubernetes_storage_class" "default" {
  metadata {
    name = "data-disk-class"
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  parameters {
    type = "pd-ssd"
  }
}
